class SingletonClass:
    _singleton_obj = None

    def __new__(cls, *args, **kwargs):
        if not cls._singleton_obj:
            cls._singleton_obj = super(SingletonClass, cls).__new__(cls, *args, **kwargs)
        return cls._singleton_obj

class GeneralClass:
    pass

def main():
    # These two objects are equal and located
    # at the same address. Thus, they are the
    # same object.
    obj_one = SingletonClass()
    obj_two = SingletonClass()


    print('Singleton Obj1 : ', obj_one)
    print('Singleton Obj2 : ', obj_two)

    # These two objects are not equal and located
    # at the different address.
    obj_one = GeneralClass()
    obj_two = GeneralClass()

    print('General Obj1 : ', obj_one)
    print('General Obj2 : ', obj_two)

if __name__ == '__main__':
    main()